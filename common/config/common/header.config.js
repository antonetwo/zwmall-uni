export default {
	osLinks: [{
			name: 'MIUI',
			url: 'https://www.miui.com'
		},
		{
			name: 'IOS',
			url: 'https://www.apple.com/cn/ios'
		},
		{
			name: 'EMUI',
			url: 'https://www.emui.com'
		},
		{
			name: 'Color OS',
			url: 'https://www.coloros.com'
		},
		{
			name: 'Funtouch OS',
			url: 'https://www.vivo.com.cn/funtouchos'
		},
		{
			name: 'H2 OS',
			url: 'https://www.h2os.com/detail'
		},
		{
			name: 'Smartisan OS',
			url: 'https://www.smartisan.com/os'
		},
		{
			name: 'Flyme',
			url: 'https://www.flyme.cn'
		}
	]
}
